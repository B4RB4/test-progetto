package paranoid.graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import paranoid.input.KeyboardInputController;
import paranoid.model.Ball;
import paranoid.model.Border;
import paranoid.model.GameObj;
import paranoid.model.Player;
import paranoid.model.Tile;

public class Scene{

    private JFrame frame;
    private Painter scene;
    private KeyboardInputController controller;
    
    public Scene(final int w, final int h, KeyboardInputController controller) {
    	this.controller = controller;
    	this.scene = new Painter(w, h);
        this.frame = new JFrame();
        frame.setSize(w,h);
        frame.setMinimumSize(new Dimension(w, h));
        frame.setResizable(false);
        frame.getContentPane().add(scene);
        frame.pack();
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent ev){
                System.exit(-1);
            }
            public void windowClosed(WindowEvent ev){
                System.exit(-1);
            }
        });
    }
    
    public void setViewObj(List<GameObj> elemnts, Border border) {
        this.scene.setViewObj(elemnts, border);
    }
    
    public void render(){
        try {
            this.scene.repaint();
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public class Painter extends JPanel implements KeyListener{

        private static final long serialVersionUID = 6161852781624671509L;
        private int sizeX;
        private int sizeY;
        private List<GameObj> elements = new ArrayList<>();
        private Border border;
        
        public Painter(final int w, final int h){
            this.sizeX = w;
            this.sizeY = h;
            this.addKeyListener(this);
        	setFocusable(true);
            setFocusTraversalKeysEnabled(false);
            requestFocusInWindow();        
        }
        
        @Override
        public void paint(Graphics g){
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2.clearRect(0,0,this.getWidth(),this.getHeight());
            g2.setColor(Color.WHITE);
            g2.fillRect(0, 0, sizeX, sizeY);
            g2.setColor(Color.BLACK);
            g2.drawRect((int)border.getUpperleftCorner().x, (int)border.getUpperleftCorner().x, 
            		    (int)border.getBottomRightCorner().x, (int)border.getBottomRightCorner().y);
            
            for(var elem : this.elements) {
                if(elem instanceof Ball) {
                    g2.setColor(Color.BLACK);
                    Ball ball = (Ball) elem;
                    g2.fillOval((int)ball.getCurrentPos().x, (int)ball.getCurrentPos().y, (int)ball.getWidth(), (int)ball.getHeight());
                } else if(elem instanceof Tile) {
                    g2.setColor(Color.RED);
                    Tile tile = (Tile) elem;
                    g2.fillRect((int)tile.getCurrentPos().x, (int)tile.getCurrentPos().y, (int)tile.getWidth(), (int)tile.getHeight());
                } else if(elem instanceof Player) {
                    g2.setColor(Color.BLUE);
                    Player player = (Player) elem;
                    g2.fillRect((int)player.getCurrentPos().x, (int)player.getCurrentPos().y, (int)player.getWidth(), (int)player.getHeight());
                }
            }
            g2.dispose();       
        }
        
        public void setViewObj(List<GameObj> elem, Border border) {
            this.elements = elem;
            this.border = border;
        }

        //---------------------------------------------------------//
        
		@Override
		public void keyTyped(KeyEvent e) {
			
		}

		@Override
		public void keyPressed(KeyEvent e) {
			if(e.getKeyCode() == 38 ) {
				controller.notifyBallRoll();
			} else if (e.getKeyCode() == 39){
	     		controller.notifyMoveRight();
	     	} else if (e.getKeyCode() == 37){
	     		controller.notifyMoveLeft();
	     	}
		}

		@Override
		public void keyReleased(KeyEvent e) {
			if (e.getKeyCode() == 38) {
	     		controller.notifyNoMoreBallRoll();
	     	} else if (e.getKeyCode() == 39){
	     		controller.notifyNoMoreMoveRight();
	     	} else if (e.getKeyCode() == 37){
	     		controller.notifyNoMoreMoveLeft();
	     	}
		}


    }
    
}
