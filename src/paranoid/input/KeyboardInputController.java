package paranoid.input;

public class KeyboardInputController implements InputController {	
	
	private boolean isMoveLeft;
	private boolean isMoveRight;
	private boolean isBallRolling;
	
	@Override
	public boolean isMoveLeft() {
		return isMoveLeft;
	}

	@Override
	public boolean isMoveRight() {
		return isMoveRight;
	}
	
	@Override
	public boolean isBallRolling() {
		return isBallRolling;
	}

	public void notifyBallRoll() {
		isBallRolling = true;
	}
	
	public void notifyNoMoreBallRoll() {
		isBallRolling = false;
	}
	
	public void notifyMoveLeft() {
		isMoveLeft = true;
	}

	public void notifyNoMoreMoveLeft() {
		isMoveLeft = false;
	}

	public void notifyMoveRight() {
		isMoveRight = true;
	}

	public void notifyNoMoreMoveRight() {
		isMoveRight = false;
	}

}
