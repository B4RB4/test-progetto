package paranoid.input;

import paranoid.model.GameObj;

public class DummyInputComponent implements InputComponent{

	@Override
	public void update(GameObj ball, InputController c) {
		//un componente di input che non fa nulla
	}

}
