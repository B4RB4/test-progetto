package paranoid.input;

import paranoid.common.P2d;
import paranoid.model.Ball;
import paranoid.model.GameObj;

public class BallInputComponent implements InputComponent{

	private static int BALL_AGILITY = 5;
	private boolean neverStarted;
	
	public BallInputComponent() {
		neverStarted = true;
	}
	
	@Override
	public void update(GameObj obj, InputController c) {	
		Ball ball = (Ball) obj;
		if(!c.isBallRolling() && neverStarted) {
			ball.setBallMotion(false);
			if (c.isMoveLeft()){
				ball.setPos(new P2d(ball.getCurrentPos().x - BALL_AGILITY, ball.getCurrentPos().y));
			} else if (c.isMoveRight()){
				ball.setPos(new P2d(ball.getCurrentPos().x + BALL_AGILITY, ball.getCurrentPos().y));
			}
		} else {
			neverStarted = false;
			ball.setBallMotion(true);
		}
	}
}
