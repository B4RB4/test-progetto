package paranoid.input;

import paranoid.common.P2d;
import paranoid.model.GameObj;

public class PlayerInputComponent implements InputComponent {
	
	private static int PLAYER_AGILITY = 10;  
	
	public void update(GameObj player, InputController ctrl){
		if (ctrl.isMoveLeft()){
			player.setPos(new P2d(player.getCurrentPos().x - PLAYER_AGILITY, player.getCurrentPos().y));
		} else if (ctrl.isMoveRight()){
			player.setPos(new P2d(player.getCurrentPos().x + PLAYER_AGILITY, player.getCurrentPos().y));
		}
	}
	
}
