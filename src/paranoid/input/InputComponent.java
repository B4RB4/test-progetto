package paranoid.input;

import paranoid.model.GameObj;

public interface InputComponent {
	
	void update(GameObj ball, InputController c);

}
