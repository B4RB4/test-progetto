package paranoid.input;

public interface InputController {

	boolean isMoveLeft();
	
	boolean isMoveRight();
	
	boolean isBallRolling();
	
}
