package paranoid.event;

import paranoid.model.Tile;

public class HitTileEvent implements Event{

    private Tile collisionObject;
    
    public HitTileEvent(final Tile collisionObject) {
        this.collisionObject = collisionObject;
    }
    
    public Tile getCollisionObject() {
        return this.collisionObject;
    }
    
}
