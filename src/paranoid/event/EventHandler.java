package paranoid.event;

import java.util.ArrayList;
import java.util.List;

import paranoid.core.GameState;

public class EventHandler {

	private GameState gameState;
	private List<Event> events = new ArrayList<>();
	
	public EventHandler(final GameState gameState) {
		this.gameState = gameState;
	}
	
	public void resolveEvent() {
		events.stream().forEach(ev -> {
            if (ev instanceof HitBorderEvent){
            	gameState.flatMultiplier();
            } else if (ev instanceof HitTileEvent){
            	gameState.incScore();
            	gameState.incMultiplier();
            	HitTileEvent hit = (HitTileEvent)ev;
            	gameState.getWorld().removeTile(hit.getCollisionObject());
            }
        });
        events.clear();
	}
	
	public void addEvent(Event event) {
		this.events.add(event);
	}
	
	
	
}
