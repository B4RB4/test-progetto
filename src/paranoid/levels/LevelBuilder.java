package paranoid.levels;

import java.util.ArrayList;
import java.util.List;

import paranoid.common.P2d;
import paranoid.model.Ball;
import paranoid.model.Border;
import paranoid.model.Player;
import paranoid.model.Tile;

public class LevelBuilder {

	private List<Ball> balls = new ArrayList<>();    
    private List<Tile> tiles = new ArrayList<>();
    private List<Player> players = new ArrayList<>();
    private Border border;
	
	public LevelBuilder(final int width, final int height) {
		this.border = new Border(new P2d(0, 0), new P2d(width, height));
	}
	
	public Level buildLevel() {
		return new Level(border, tiles, balls, players); 
	}
	
	public LevelBuilder addBalls(List<Ball> balls) {
    	this.balls.addAll(balls);
    	return this;
    }
    
    public LevelBuilder addBall(Ball ball) {
    	this.balls.add(ball);
    	return this;
    }

    public LevelBuilder addTiles(List<Tile> tiles) {
    	this.tiles.addAll(tiles);
    	return this;
    }
    
    public LevelBuilder addTiles(Tile tile) {
    	this.tiles.add(tile);
    	return this;
    }
    
    public LevelBuilder addPlayers(List<Player> players) {
    	this.players.addAll(players);
    	return this;
    }
    
    public LevelBuilder addPlayer(Player player) {
    	this.players.add(player);
    	return this;
    }
    
    public LevelBuilder removePlayer(Player player) {
    	this.players.remove(player);
    	return this;
    }
}
