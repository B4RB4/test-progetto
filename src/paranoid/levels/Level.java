package paranoid.levels;

import java.util.ArrayList;
import java.util.List;
import paranoid.model.Ball;
import paranoid.model.Border;
import paranoid.model.Player;
import paranoid.model.Tile;

public class Level {
	
	private List<Ball> balls = new ArrayList<>(); 
    private List<Tile> tiles = new ArrayList<>();
    private List<Player> players = new ArrayList<>();
    private Border border;
	
	public Level(Border border, List<Tile> tiles, List<Ball> balls, List<Player> players) {
		this.border = border;
		this.tiles.addAll(tiles);
		this.balls.addAll(balls);
		this.players.addAll(players);
	}

	public List<Ball> getBalls() {
		return balls;
	}

	public List<Tile> getTiles() {
		return tiles;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public Border getBorder() {
		return border;
	}
}
