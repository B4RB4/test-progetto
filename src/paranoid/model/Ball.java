package paranoid.model;

import paranoid.common.P2d;
import paranoid.common.V2d;
import paranoid.input.BallInputComponent;
import paranoid.input.InputController;
import paranoid.physics.BallPhysicsComponent;

public class Ball extends GameObj{

	private boolean isMoving;
	
    public Ball(P2d pos, V2d vel, int height, int width, boolean isMoving) {
        super(pos, vel, height, width, new BallPhysicsComponent(), new BallInputComponent());
        this.isMoving = isMoving; 
    }
    
    public void setBallMotion(boolean newState) {
    	this.isMoving = newState;
    }
    
    public boolean getBallMotion() {
    	return this.isMoving;
    }
    
    public void flipVelOnY(){
        this.setVel(new V2d(this.getCurrentVel().x, -this.getCurrentVel().y));
    }

    public void flipVelOnX(){
        this.setVel(new V2d(-this.getCurrentVel().x, this.getCurrentVel().y));
    }
    
    public void flipByValue(V2d newValue){
        this.setVel(newValue);
    }
    
    @Override
    public void updatePhysics(int dt, World w) {
        if(isMoving) {
        	this.getPhysicsComponent().update(dt, this, w);
        }
    }

	@Override
	public void updateInput(GameObj player, InputController controller) {
		this.getInputComponent().update(this, controller);
	}

}
