package paranoid.model;

import paranoid.common.P2d;
import paranoid.common.V2d;
import paranoid.input.InputComponent;
import paranoid.input.InputController;
import paranoid.physics.PhysicsComponent;

public abstract class GameObj {

    private P2d pos;
    private V2d vel;
    private int height;
    private int width;
    private PhysicsComponent phys;
    private InputComponent input;
    
    public GameObj(P2d pos, V2d vel, int height, int width, PhysicsComponent phys, InputComponent input){
            this.pos = pos;
            this.vel = vel;
            this.height = height;
            this.width = width;
            this.phys = phys;
            this.input = input;
    }
    
    public void setPos(P2d pos){
            this.pos = pos;
    }

    public void setVel(V2d vel){
            this.vel = vel;
    }
    
    public P2d getCurrentPos(){
            return pos;
    }
    
    public V2d getCurrentVel(){
            return vel;
    }

    public int getHeight() {
        return this.height;
    }
    
    public double getWidth() {
        return this.width;
    }
    
    public PhysicsComponent getPhysicsComponent() {
        return this.phys;
    }
    
    public InputComponent getInputComponent() {
        return this.input;
    }
    
    abstract public void updatePhysics(int dt, World w);
    
    abstract public void updateInput(GameObj player, InputController controller);
    
    
}
