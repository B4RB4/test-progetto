package paranoid.model;

import java.io.Serializable;
import java.util.Optional;

import paranoid.common.Collision;
import paranoid.common.P2d;
import paranoid.common.V2d;
import paranoid.input.DummyInputComponent;
import paranoid.input.InputController;
import paranoid.physics.DummyPhysicsComponent;
import paranoid.physics.PhysicsComponent;

public class Tile extends GameObj implements Serializable{

	private static final long serialVersionUID = -8756897843366807316L;
	
	private Optional<Collision> lastZonePresence;
	private int pointsEarned;
	
	public Tile(P2d pos, int height, int width, int pointsEarned, PhysicsComponent phys) {
		super(pos, new V2d(0, 0), height, width, new DummyPhysicsComponent(), new DummyInputComponent());
		this.lastZonePresence = Optional.empty();
		this.pointsEarned = pointsEarned;
	}

	@Override
	public void updatePhysics(int dt, World w) {
		this.getPhysicsComponent().update(dt, this, w);
	}
	
	@Override
	public void updateInput(GameObj player, InputController controller) {
		this.getInputComponent().update(this, controller);
	}
	
	public void setLastZonePresence(Collision collision) {
		this.lastZonePresence = Optional.of(collision);
	}
	
	public Optional<Collision> getLastZonePresence() {
		return this.lastZonePresence;
	}
	
	public int getPointsEarned() {
		return this.pointsEarned;
	}

}
