package paranoid.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import paranoid.common.Collision;
import paranoid.common.Direction;
import paranoid.common.Pair;
import paranoid.event.Event;
import paranoid.input.InputController;
import paranoid.levels.Level;

public class World {

    private List<Ball> balls = new ArrayList<>();    
    private List<Tile> tiles = new ArrayList<>();
    private List<Player> players = new ArrayList<>();    
	private Border border;
	private WorldEventListener eventListener;
	private CollisionManager collisionManager;

	public World(final WorldEventListener eventListener, final Level level) {
		this.eventListener = eventListener;
		this.balls = level.getBalls();
		this.tiles = level.getTiles();
		this.players = level.getPlayers();
		this.border = level.getBorder();
		this.collisionManager = new CollisionManager();
	}
	
	public Optional<Collision> checkCollisionWithBoundaries(GameObj entity){
    	return this.collisionManager.checkCollisionWithBoundaries(border, entity);
    }
    
    public Optional<Pair<Tile, Collision>> checkCollisionWithTiles(GameObj entity){
    	Optional<Pair<Tile, Collision>> collisionResult = Optional.empty();
    	for(Tile tile : this.tiles) {
    		collisionResult = this.collisionManager.checkCollisionWithTiles(tile, entity);
    		if(collisionResult.isPresent()) {
    			return collisionResult;
    		}
    	}
    	return collisionResult; 
    }
    
    public Optional<Direction> checkCollisionWithPlayers(GameObj entity) {
    	Optional<Direction> collisionResult = Optional.empty();
    	for(Player player : this.players) {
    		collisionResult = this.collisionManager.checkCollisionWithPlayers(player, entity);
    		if(collisionResult.isPresent()) {
    			return collisionResult;
    		}
    	}
    	return Optional.empty();
    }
	    
    public void updateState(int dt){
    	this.getSceneEntities().forEach(i -> i.updatePhysics(dt, this));
    }
    
    public void movePayer(InputController controller) {
    	this.getSceneEntities().forEach(i -> i.updateInput(i, controller));
    }

    public Border getBorder() {
        return this.border;
    }
    
    public void removeTile(Tile tile) {
    	this.tiles.remove(tile);
    }
    
    public void notifyWorldEvent(Event ev) {
        this.eventListener.notifyEvent(ev);
    }
    
    public List<GameObj> getSceneEntities(){
        List<GameObj> entities = new ArrayList<GameObj>();
        entities.addAll(balls);
        entities.addAll(players);
        entities.addAll(tiles);
        return entities;
    }
    
    public boolean isGameOver() {
    	return this.balls.stream().allMatch(i -> i.getCurrentPos().y > border.getBottomRightCorner().y);
    }
    
}
