package paranoid.model;

import paranoid.common.P2d;
import paranoid.common.V2d;
import paranoid.input.InputController;
import paranoid.input.PlayerInputComponent;
import paranoid.physics.PlayerPhysicsComponent;

public class Player extends GameObj{
	
	public Player(P2d pos, int height, int width) {
		super(pos, new V2d(0, 0), height, width, new PlayerPhysicsComponent(), new PlayerInputComponent());
	}

	@Override
    public void updatePhysics(int dt, World w) {
        this.getPhysicsComponent().update(dt, this, w);
    }

	@Override
	public void updateInput(GameObj player, InputController controller) {
		this.getInputComponent().update(this, controller);
	}
	
}
