package paranoid.model;

import java.util.Optional;
import paranoid.common.Collision;
import paranoid.common.Direction;
import paranoid.common.Pair;

public class CollisionManager {
	
	public CollisionManager() {}
	
	
	public Optional<Collision> checkCollisionWithBoundaries(Border border, GameObj entity){
    	if (entity.getCurrentPos().y < border.getUpperleftCorner().y){
            return Optional.of(Collision.ORIZONTAL);
        } else if (entity.getCurrentPos().x < border.getUpperleftCorner().x){
            return Optional.of(Collision.VERTICAL);
        } else if (entity.getCurrentPos().x + entity.getWidth() > border.getBottomRightCorner().x){
            return Optional.of(Collision.VERTICAL);
        } else {
            return Optional.empty();
        }
    }
    
    public Optional<Pair<Tile, Collision>> checkCollisionWithTiles(Tile tile, GameObj entity){
    	boolean checkLeft = false;
        boolean checkRight = false;
        boolean checkBot = false;
        boolean checkTop = false;
        if (entity.getCurrentPos().x <= tile.getCurrentPos().x + tile.getWidth()) {
            checkLeft = true;
        } else {
            tile.setLastZonePresence(Collision.VERTICAL);
        }
        if (entity.getCurrentPos().x + entity.getWidth() >= tile.getCurrentPos().x) {
            checkRight = true;
        } else {
        	tile.setLastZonePresence(Collision.VERTICAL);
        }
        if (entity.getCurrentPos().y + entity.getHeight() >= tile.getCurrentPos().y) {
            checkBot = true;
        } else {
        	tile.setLastZonePresence(Collision.ORIZONTAL);
        }
        if (entity.getCurrentPos().y <= tile.getCurrentPos().y + tile.getHeight()) {
            checkTop = true;
        } else {
        	tile.setLastZonePresence(Collision.ORIZONTAL);
        }
        
        if (checkLeft && checkRight && checkBot && checkTop) {
        	return Optional.of(new Pair<>(tile, tile.getLastZonePresence().get()));
        }
        return Optional.empty();
    }
    
    public Optional<Direction> checkCollisionWithPlayers(Player player, GameObj entity) {
    	if(player.getCurrentPos().x < entity.getCurrentPos().x + entity.getWidth() &&
    		player.getCurrentPos().x + player.getWidth() > entity.getCurrentPos().x &&
    		player.getCurrentPos().y < entity.getCurrentPos().y + entity.getHeight() &&
    		player.getCurrentPos().y + player.getHeight() > entity.getCurrentPos().y){    			
    		int CenterOfGravity = (int) (entity.getCurrentPos().x + (entity.getWidth()/2));
    		int zone = (int) (player.getWidth()/4);
    		if(CenterOfGravity < player.getCurrentPos().x + (zone*1) && CenterOfGravity > player.getCurrentPos().x){
    			return Optional.of(Direction.LEFT_EDGE);
    		} else if(CenterOfGravity < player.getCurrentPos().x + (zone*2) && CenterOfGravity > player.getCurrentPos().x + (zone*1)) {
    			return Optional.of(Direction.LEFT);
    		} else if(CenterOfGravity < player.getCurrentPos().x + (zone*3) && CenterOfGravity > player.getCurrentPos().x + (zone*2)) {
    			return Optional.of(Direction.RIGHT);
    		} else if(CenterOfGravity < player.getCurrentPos().x + (zone*4) && CenterOfGravity > player.getCurrentPos().x + (zone*3)) {
    			return Optional.of(Direction.RIGHT_EDGE);
    		}	
    	}
    	return Optional.empty();
    }
}
