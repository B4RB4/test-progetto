package paranoid.model;

import paranoid.common.P2d;

public class Border {

    private P2d upperLefCorner;
    private P2d bottomRightCorner;
    
    public Border(P2d upperLeftCorner, P2d bottomRightCorner) {
        this.bottomRightCorner = bottomRightCorner;
        this.upperLefCorner = upperLeftCorner;
    }
    
    public P2d getBottomRightCorner() {
        return this.bottomRightCorner;
    }
    
    public P2d getUpperleftCorner() {
        return this.upperLefCorner;
    }
    
    public int getHeight() {
        return (int) (bottomRightCorner.x - upperLefCorner.x);
    }
    
    public double getWidth() {
        return (int) (bottomRightCorner.y - upperLefCorner.y);
    }
    
}
