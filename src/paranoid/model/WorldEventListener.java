package paranoid.model;

import paranoid.event.Event;

public interface WorldEventListener {

    void notifyEvent(Event ev);
    
}
