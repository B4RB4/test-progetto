package paranoid.main;

import paranoid.core.GameLoop;

public class Main {

    public static void main(String[] args) {
        GameLoop gameLoop = new GameLoop();
        while(true) {
        	gameLoop.mainLoop();
        }
    }
}
