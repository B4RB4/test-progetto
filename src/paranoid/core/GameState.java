package paranoid.core;

import java.util.ArrayList;
import java.util.List;

import paranoid.common.P2d;
import paranoid.common.V2d;
import paranoid.levels.LevelBuilder;
import paranoid.model.Ball;
import paranoid.model.Player;
import paranoid.model.Tile;
import paranoid.model.World;
import paranoid.model.WorldEventListener;
import paranoid.physics.DummyPhysicsComponent;

public class GameState {
	
	private static int PROGRESS = 100;

    private int score;
    private int multiplier;
    private int lives;
    
    private World world;
    private WorldEventListener listener;
    
    public GameState(WorldEventListener listener){
    	this.listener = listener;
    }
    
    public void gameInit() {
    	this.score = 0;
        this.multiplier = 1;
        this.lives = 1;
        
        LevelBuilder levelBuilder = new LevelBuilder(500, 500);
        
        List<Player> playerContainer = new ArrayList<>();
        playerContainer.add(new Player(new P2d(250, 490), 10, 100));
        
        List<Ball> ballContainer = new ArrayList<>();
        ballContainer.add(new Ball(new P2d(275, 475), new V2d(300, 300), 10, 10, false));
        
        List<Tile> tileContainer = new ArrayList<>();
        tileContainer.add(new Tile(new P2d(10, 10), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(90, 10), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(170, 10), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(250, 10), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(330, 10), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(410, 10), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(10, 50), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(90, 50), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(170, 50), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(250, 50), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(330, 50), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(410, 50), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(10, 90), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(90, 90), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(170, 90), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(250, 90), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(330, 90), 30, 70, 10, new DummyPhysicsComponent()));
        tileContainer.add(new Tile(new P2d(410, 90), 30, 70, 10, new DummyPhysicsComponent()));
        
        levelBuilder.addTiles(tileContainer)
        			.addBalls(ballContainer)
        			.addPlayers(playerContainer);
        
        world = new World(this.listener, levelBuilder.buildLevel());
    }
    
    public World getWorld(){
    	return world;
    }
    
    public void incMultiplier(){
        this.multiplier++;
    }

    public void flatMultiplier(){
        this.multiplier = 1;
    }
    
    public void incScore(){
        this.score += PROGRESS * multiplier;
    }

    public void decScore(){
        this.score -= PROGRESS;
    }
    
    public int getScore(){
        return this.score;
    }
    
    public int getMultiplier(){
        return this.multiplier;
    }
    
    public int getLives(){
    	return this.lives;
    }
    
    public void update(int dt){
            world.updateState(dt);
    }
    
}
