package paranoid.core;

import paranoid.event.Event;
import paranoid.event.EventHandler;
import paranoid.graphics.Scene;
import paranoid.input.KeyboardInputController;
import paranoid.model.WorldEventListener;

public class GameLoop implements WorldEventListener{

    private long period = 20;   
    private GameState gameState;
    private Scene scene;
    private KeyboardInputController controller;
    private EventHandler eventHandler;
    
    public GameLoop(){
    	//this.events = new ArrayList<Event>();
        this.gameState = new GameState(this);
        this.controller = new KeyboardInputController();
        this.scene = new Scene(517, 540, controller);
        this.eventHandler = new EventHandler(gameState);
    }       
    
    public void mainLoop(){
    	this.gameState.gameInit();
    	long lastTime = System.currentTimeMillis();
    	while(!gameState.getWorld().isGameOver()){
    		long current = System.currentTimeMillis();
    		int elapsed = (int)(current - lastTime);
    		processInput();
    		updateGame(elapsed);
    		render();
    		waitForNextFrame(current);
    		lastTime = current;
    	}
    }

    private void waitForNextFrame(long current){
            long dt = System.currentTimeMillis() - current;
            if (dt < period){
                    try {
                            Thread.sleep(period-dt);
                    } catch (Exception ex){}
            }
    }
    
    private void processInput(){
    	gameState.getWorld().movePayer(controller);
    }
    
    private void updateGame(int elapsed){
        this.gameState.getWorld().updateState(elapsed);
        this.eventHandler.resolveEvent();
        //checkEvents();
    }
        
    /*
    private void checkEvents(){
    	events.stream().forEach(ev -> {
            if (ev instanceof HitBorderEvent){
            	gameState.flatMultiplier();
            } else if (ev instanceof HitTileEvent){
            	gameState.incScore();
            	gameState.incMultiplier();
            	HitTileEvent hit = (HitTileEvent)ev;
            	gameState.getWorld().removeTile(hit.getCollisionObject());
            }
        });
        events.clear();
    }
    */
    
    private void render(){
    	scene.setViewObj(gameState.getWorld().getSceneEntities(), gameState.getWorld().getBorder());
    	scene.render();
    }

    @Override
    public void notifyEvent(Event ev) {
        //this.events.add(ev);
    	this.eventHandler.addEvent(ev);
    }

    
}
