package paranoid.common;

public enum Direction {
    
    RIGHT, RIGHT_EDGE, LEFT, LEFT_EDGE;

}
