package paranoid.physics;

import paranoid.model.GameObj;
import paranoid.model.World;

public class DummyPhysicsComponent implements PhysicsComponent{

	@Override
	public void update(int dt, GameObj gameObj, World w) {
		//un componente fisico che non fa nulla
	}

}
