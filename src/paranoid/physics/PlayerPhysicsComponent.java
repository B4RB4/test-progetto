package paranoid.physics;

import paranoid.common.P2d;
import paranoid.model.GameObj;
import paranoid.model.World;

public class PlayerPhysicsComponent implements PhysicsComponent{

	@Override
	public void update(int dt, GameObj gameObj, World w) {
		if(gameObj.getCurrentPos().x < w.getBorder().getUpperleftCorner().x) {
			gameObj.setPos(new P2d(w.getBorder().getUpperleftCorner().x, gameObj.getCurrentPos().y));
		}
		if(gameObj.getCurrentPos().x + gameObj.getWidth() > w.getBorder().getBottomRightCorner().x) {
			gameObj.setPos(new P2d(w.getBorder().getBottomRightCorner().x - gameObj.getWidth(), gameObj.getCurrentPos().y));
		}
	}

}
