package paranoid.physics;

import paranoid.model.GameObj;
import paranoid.model.World;

public interface PhysicsComponent {
    
    public void update(int dt, GameObj gameObj, World w);

}
