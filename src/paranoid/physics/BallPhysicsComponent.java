package paranoid.physics;

import java.util.Optional;

import paranoid.common.Collision;
import paranoid.common.Direction;
import paranoid.common.P2d;
import paranoid.common.Pair;
import paranoid.common.V2d;
import paranoid.event.HitBorderEvent;
import paranoid.event.HitTileEvent;
import paranoid.model.Ball;
import paranoid.model.GameObj;
import paranoid.model.Tile;
import paranoid.model.World;

public class BallPhysicsComponent implements PhysicsComponent{

    private static double SCALER = 0.001;
    
    @Override
    public void update(int dt, GameObj boundingObject, World w) {
    	P2d oldPos = boundingObject.getCurrentPos();
    	
    	Ball ball = (Ball) boundingObject;
        P2d pos = ball.getCurrentPos();
        V2d vel = ball.getCurrentVel();
        ball.setPos(pos.sum(vel.mul(SCALER*dt)));

        Optional<Collision> borderCollisionInfo = w.checkCollisionWithBoundaries(boundingObject);
        if(borderCollisionInfo.isPresent()) {
        	ball.setPos(oldPos);
            if(borderCollisionInfo.get().equals(Collision.ORIZONTAL)) {
                ball.flipVelOnY();
            } else {
                ball.flipVelOnX();
            }
            w.notifyWorldEvent(new HitBorderEvent());
        }
        
        Optional<Pair<Tile, Collision>> tileCollisionInfo = w.checkCollisionWithTiles(boundingObject);
        if(tileCollisionInfo.isPresent()) {
        	ball.setPos(oldPos);
        	Pair<Tile, Collision> content = tileCollisionInfo.get();
        	if(content.getY().equals(Collision.ORIZONTAL)) {
                ball.flipVelOnY();
            } else {
                ball.flipVelOnX();
            }
        	w.notifyWorldEvent(new HitTileEvent(content.getX()));
        }
        
        Optional<Direction> playerInfo = w.checkCollisionWithPlayers(boundingObject);
        if(playerInfo.isPresent()) {
        	ball.setPos(oldPos);        	
        	ball.flipVelOnY();
        	if(playerInfo.get().equals(Direction.LEFT_EDGE)) {
        		ball.setVel(new V2d(-400, -100));
        	} else if(playerInfo.get().equals(Direction.LEFT)) {
        		ball.setVel(new V2d(-300, -300));
        	} else if(playerInfo.get().equals(Direction.RIGHT)) {
        		ball.setVel(new V2d(300, -300));
        	} else if(playerInfo.get().equals(Direction.RIGHT_EDGE)) {
        		ball.setVel(new V2d(400, -100));
        	}
        }
    }
}
